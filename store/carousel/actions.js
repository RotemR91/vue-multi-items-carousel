const actions = {
	setCarouselItems({commit}, v){
		commit('setCarouselItems', v)
	}
}
export default actions
