import Vue from 'vue'

const mutations = {
	setCarouselItems(state, v){
		Vue.set(state, 'carouselItemsNum', v)
	}
}

export default mutations
