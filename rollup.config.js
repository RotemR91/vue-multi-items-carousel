// rollup.config.js
import vue from 'rollup-plugin-vue';
import babel from 'rollup-plugin-babel';
import uglify from 'rollup-plugin-uglify-es';
import minimist from 'minimist';
import async from 'rollup-plugin-async';

import commonjs from 'rollup-plugin-commonjs';
import nodeResolve from 'rollup-plugin-node-resolve';
import postcss from 'rollup-plugin-postcss'

import css from 'rollup-plugin-css-only'
import pkg from './package.json';

const argv = minimist(process.argv.slice(2));

const config = {
	input: './entry.js',

	external: [
		'vue',
		'vuex',
	],
	output: {
		name: pkg.name,
		exports: 'named',
		globals: {
			'vue':     'vue' ,
			'vuex':    'vuex' ,
		},
	},
	plugins: [
		css(),
		vue({
			css: false,
			compileTemplate: true,
			template: { optimizeSSR: false },
		}),
		async(),
		postcss(),


		nodeResolve({
			mainFields: [
				'module', 'main', 'jsnext',
			]
		}),
		commonjs({
			// non-CommonJS modules will be ignored, but you can also
			// specifically include/exclude files
			include: 'node_modules/**',  // Default: undefined

			// search for files other than .js files (must already
			// be transpiled by a previous plugin!)
			extensions: [ '.js', '.coffee' ],  // Default: [ '.js' ]


			namedExports: {
				'./store/carousel/index.js': ['module' ] ,
			},  // Default: undefined

			// sometimes you have to leave require statements
			// unconverted. Pass an array containing the IDs
			// or a `id => boolean` function. Only use this
			// option if you know what you're doing!
			ignore: [ 'conditional-runtime-dependency' ]
		}),
		babel({
			exclude: 'node_modules/**',
			externalHelpers: true,
			runtimeHelpers: true,
			plugins: [
				['wildcard', { exts: ['vue'], nostrip: true, },],
				// '@babel/plugin-external-helpers',
				// '@babel/plugin-transform-runtime'

			],
			presets: [
				['@babel/preset-env', { modules: false, },],
			],
		}),

	],
};


// Only minify browser (iife) version
if (argv.format === 'iife') {
	config.plugins.push(uglify());
}

export default config;
