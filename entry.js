import Vue from 'vue';

import module from './store/carousel/index.js'
import multiItemsCarousel from './components/multiItemsCarousel.vue'

const components = {
	multiItemsCarousel
}

function install(Vue) {
	if (install.installed) return;
	install.installed = true;

	Object.keys(components).forEach(name => {
		Vue.component(name, components[name])
	});

}

const plugin = {
	install
}

let GlobalVue = null;
if (typeof window !== 'undefined') {
	GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
	GlobalVue = global.Vue;
}
if (GlobalVue) {
	GlobalVue.use(plugin);
}

export default components
export const strict = false

export {
	module,
	multiItemsCarousel
}
