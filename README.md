# vue-multi-items-carousel (vmic)

Multi items carousel based on vuetifys v-carousel and scoped slots for custom components

Installation:
------
```
npm i vmic
```
Initiate the store and create a folder (module) named carousel, and in it a **index.js** file:

```javascript
import { module } from "vmic";

export default module;
```

Import the carousel.css:

* Using Nuxt:
In nuxt.config.js
```javascript
css: ["vmic/assets/carousel.css"]
```
* ES15/16:
```javascript
import "vmic/assets/carousel.css"
```

Example:
------

```html
<template>
	<v-col
		justify-center
		align-center
	>
		<multiItemsCarousel
			:key="nitems"
			:items="items"
			:itemsNum="nitems"
			:per-click="2"
			:height="height"
			:interval="inter"
			:cycle="false"
			progress
			progress-color="red"
		>
			<template slot-scope="props">
				<v-img v-if="type === 'images'" :src="props.url" :width="width" :height="height-50" class="bfront"></v-img>
				<div v-else-if="type === 'text'">{{props.text}}</div>
				<NewsCard v-else-if="type === 'cards'" v-bind="props"/>
			</template>
		</multiItemsCarousel>
	</v-layout>
</template>
```

multiItemsCarousel props:
------

```javascript
props:{
	items:{
		type: Array,
		required: true
	},
	height:{
		type: [Number, String],
		default: 500
	},
	itemsNum:{
		type: [Number, String],
		default: 3
	},
	cycle:{
		type: Boolean,
		default: true
	},
	interval:{
		type: Number,
		default: 10000
	},
	progress:{
		type: Boolean,
		default: true
	},
	hideDelimiters:{
		type: Boolean,
		default: false
	},
	progressColor:{
		type: String,
		default: 'light-blue'
	},
	perClick:{
		type: [Number, String],
		defualt: 1
	}
}

Live Demo: https://codesandbox.io/s/vmic-test-7yxxc
